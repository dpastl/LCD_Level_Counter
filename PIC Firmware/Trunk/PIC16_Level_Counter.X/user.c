/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "user.h"
#include "lcd.h"

unsigned int currentLife = 0;
unsigned char	upButtonFlag = 0;
unsigned char	downButtonFlag = 0;
unsigned char	powerButtonFlag = 0;

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */

void InitApp(void)
{
    /* TODO Initialize User Ports/Peripherals/Project here */


    //Port A Setup
    PORTA = 0x00;       //Clear Port A
    TRISA = 0x00;       //Set al PORT A as output
    //TODO: Setup interrupt intput pins

    //PORT B Setup
    PORTB = 0x00;			//Clear PORT B
    TRISB = 0b00100110;     //Set all PORT B as output except RB1 and RB2
    ANSELB = 0x00;			//set all portb input as digital
    WPUB = 0b00100110;      //Set weak pull ups for the button pins

    OPTION_REGbits.nWPUEN = 0;  //Global pullup

    //PORT C Setup
    PORTC = 0x00;       //Clear PORT C
    TRISC = 0x00;       //Set all PORT C as output

#ifdef _PIC16F1934_H_
	PORTD = 0x00;
	TRISD = 0x00;
#endif
    /* Initialize peripherals */
	//LCD Configure
    //Set LCD Phase info and stuff
	LCDPSbits.LP = 0x0;		//set to 1:16 pre-scaler
	LCDPSbits.WFT = 0;		//Waveform type A
	LCDPSbits.BIASMD = 0;	//Set to 0 for static mode
	LCDPSbits.LCDA = 1;		//Set LCD driver module to active
	LCDPSbits.WA = 1;		//allow writing to the LCDDATAn registers

	//LCDCON settings
	LCDCONbits.LMUX = 0b00;		//set to static, only use COM0
	LCDCONbits.CS = 0b10;		//select LFINTOSC (31kHz) source
	LCDCONbits.SLPEN = 0;		//Enable LCD module in sleep mode

	//Enable LCD Segments on Pins TODO: Change to reflect current pin connections
#ifdef _PIC16F1934_H_
	LCDSE0 = 0b10110000;		//Enable Segment pins 0-7
	LCDSE1 = 0b10010111;		//Enable Segment pins 8-15
	LCDSE2 = 0b11111111;		//Enable Segment pins 16-23
#endif
#ifdef _PIC16F1933_H_
	LCDSE0 = 0b10111111;		//Enable Segment pins 0-7
	LCDSE1 = 0b11111111;		//Enable Segment pins 8-15
#endif
	//Write inital values to the LCDData registers


	LCDDATA0 = 0x00;
	LCDDATA1 = 0x00;
#ifdef _PIC16F1934_H_
	LCDDATA2 = 0x00;
#endif

	//Clear and disable lcd interrupt
	PIR2bits.LCDIF = 0;	//clear lcd interupt bit
	PIE2bits.LCDIE = 0;	//disable lcd interrupt

	//Configure bias voltages and such
	//LCDREF register
	LCDREFbits.VLCD3PE = 1;	//enable VLCD3 pin as input

	//LCD Contrast settings
	//LCDCSTbits.LCDCST = 0b000;	//maximum contrast, most power used
	LCDCSTbits.LCDCST = 0b111;	//minimum contrast, least power used

	//Turn on LCD
	LCDCONbits.LCDEN = 1;	//turn on the lcd

    /* Enable interrupts */
	IOCBN = 0b00100110;		//Set pins RB0 and RB1 as falling edge interrupt
	INTCONbits.IOCIE = 1;   //allow interrupts on change
	INTCONbits.GIE = 1;		//Enable global interrupts
}

void delay(unsigned long delayTime) {
    long counter = 0;
    for (counter = 0; counter<delayTime; counter++) {
        ;
    }
}

int checkButton(void)
{
	if(upButtonFlag == 1)
	{
		if(LCDCONbits.LCDEN == 0)
			LCDCONbits.LCDEN = 1;	//turn on the lcd
		else
			num2Screen(increaseLife());
                upButtonFlag = 0;
	}
	if(downButtonFlag == 1)
	{
		if(LCDCONbits.LCDEN == 0)
			LCDCONbits.LCDEN = 1;	//turn on the lcd
		else
			num2Screen(decreaseLife());
                downButtonFlag = 0;
	}
	if(powerButtonFlag == 1)
	{
		num2Screen(resetLife());
		if(LCDCONbits.LCDEN == 1)
			LCDCONbits.LCDEN = 0;	//turn off the lcd
		else
			LCDCONbits.LCDEN = 1;	//turn on the lcd
                powerButtonFlag = 0;
	}
}

void num2Screen (unsigned int value)
{
	int lsdigit = 0;
	int msdigit = 0;

	//Turn off all digits
	LCDDATA0 = 0x00;
	LCDDATA1 = 0x00;
#ifdef _PIC16F1934_H_
	LCDDATA2 = 0x00;
#endif

	//Check the input
	if (value > 99)
	{
		LCD3G = LCD2G = 1;	//print two dashes if number is too large
		return;
	}

	//get the most significant digit
	msdigit = value/10;
	//get the least significant digit
	lsdigit = value - msdigit*10;

	//Write first digit
	switch(lsdigit)
	{
		case 0:	//ABCDEF
			LCD3A = LCD3B = LCD3C = LCD3D = LCD3E = LCD3F = 1;
			break;
		case 1:	//bc
			LCD3B = LCD3C = 1;
			break;
		case 2: //abdeg
			LCD3A = LCD3B = LCD3D = LCD3E = LCD3G = 1;
			break;
		case 3:	//abcdg
			LCD3A = LCD3B = LCD3C = LCD3D = LCD3G = 1;
			break;
		case 4: //bcfg
			LCD3B = LCD3C = LCD3F = LCD3G = 1;
			break;
		case 5:	//ACDFG
			LCD3A = LCD3C = LCD3D = LCD3F = LCD3G = 1;
			break;
		case 6:	//CDEFG
			LCD3C = LCD3D = LCD3E = LCD3F = LCD3G = LCD3A = 1;
			break;
		case 7:	//ABC
			LCD3A = LCD3B = LCD3C = 1;
			break;
		case 8:	//ABCDEFG
			LCD3A = LCD3B = LCD3C = LCD3D = LCD3E = LCD3F = LCD3G = 1;
			break;
		case 9:	//ABCFG
			LCD3A = LCD3B = LCD3C = LCD3F = LCD3G = 1;
			break;
	}
	switch(msdigit)
	{
		case 0:	//ABCDEF
			LCD2A = LCD2B = LCD2C = LCD2D = LCD2E = LCD2F = 1;
			break;
		case 1:	//bc
			LCD2B = LCD2C = 1;
			break;
		case 2: //abdeg
			LCD2A = LCD2B = LCD2D = LCD2E = LCD2G = 1;
			break;
		case 3:	//abcdg
			LCD2A = LCD2B = LCD2C = LCD2D = LCD2G = 1;
			break;
		case 4: //bcfg
			LCD2B = LCD2C = LCD2F = LCD2G = 1;
			break;
		case 5:	//ACDFG
			LCD2A = LCD2C = LCD2D = LCD2F = LCD2G = 1;
			break;
		case 6:	//CDEFG
			LCD2C = LCD2D = LCD2E = LCD2F = LCD2G = LCD2A = 1;
			break;
		case 7:	//ABC
			LCD2A = LCD2B = LCD2C = 1;
			break;
		case 8:	//ABCDEFG
			LCD2A = LCD2B = LCD2C = LCD2D = LCD2E = LCD2F = LCD2G = 1;
			break;
		case 9:	//ABCFG
			LCD2A = LCD2B = LCD2C = LCD2F = LCD2G = 1;
			break;
	}
	IOCBNbits.IOCBN0 = 1;	//enable negative cahnge interrupt on RB0
	INTCONbits.GIE = 1;		//enable global interrupts
	INTCONbits.INTE = 1;	//enable interrupt on change interrupts
	INTEDG = 1;
}
unsigned int increaseLife(void)
{
	currentLife++;
	if (currentLife > 99)
		currentLife = 0;
	return currentLife;
}
unsigned int decreaseLife(void)
{
	if (currentLife > 0)
		currentLife--;
	return currentLife;
}
unsigned int zeroLife(void)
{
	currentLife = 0;
	return currentLife;
}
unsigned int resetLife(void)
{
	currentLife = 20;
	return currentLife;
}
void allOn(void)
{
	LCDDATA0 = 0xFF;
	LCDDATA1 = 0xFF;
#ifdef _PIC16F1934_H_
	LCDDATA2 = 0xFF;
#endif
}
void setUpButtonFlag(void)
{
	upButtonFlag = 1;
}
void setDownButtonFlag(void)
{
	downButtonFlag = 1;
}
void setPowerButtonFlag(void)
{
	powerButtonFlag = 1;
}