/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

/* TODO Application specific user parameters used in user.c may go here */
#define LEDpin PORTDbits.RD1

//Up button, RB1
#define UP_PIN  PORTBbits.RB1
#define UP_PIN_INT_FLAG  IOCBFbits.IOCBF1

//Down button, RB2
#define DOWN_PIN    PORTBbits.RB2
#define DOWN_PIN_INT_FLAG    IOCBFbits.IOCBF2

//Power button, RB5
#define POWER_PIN   PORTBbits.RB5
#define POWER_PIN_INT_FLAG  IOCBFbits.IOCBF5


/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

/* TODO User level functions prototypes (i.e. InitApp) go here */

void InitApp(void);         /* I/O and Peripheral Initialization */
void delay(unsigned long delayTime);
int checkButton(void);
void num2Screen (unsigned int value);
unsigned int increaseLife(void);
unsigned int decreaseLife(void);
unsigned int zeroLife(void);
unsigned int resetLife(void);
void allOn(void);
void setUpButtonFlag(void);
void setDownButtonFlag(void);
void setPowerButtonFlag(void);